# WordComplete

Word complete repo is meant for demo app . Application performs AutoComplete like feature based on set of words available 
as a source file(in Assets). Based on available match(es), the suggestion are shown in a list.
When user types in the edit field suggest logic works in background to get suggestion based on input entry.

This app also provides result for word suggestion. Example of this can be seen with  project at repo : git@bitbucket.org:AbhiHolkar/getsuggest.git.
Please follow the guide lines in repo.



# Application Architecture.
- Application uses MVP architecture & clean architecture & principles of SOLID programming.
- Dagger 2 is used for dependeny injection and ButterKnife for view injections.
- RxJava2 is used for background/asychronous operations like file loading/word search/suggest etc.
- The set of words are defined in a file as JSON and has JSON arrays for each Character (A,B,...Z)
- GSON library is used for parsing of word file.
- Application uses Mockito for Junit test cases.
- Repository and use case concept of clean architecture makes code modular and testable.
- Repository loads the set of words from file but in future in can also be loaded from Network.

# Prerequiste
- The set of words are defined in a file as JSON and has JSON arrays for each Character (A,B,...Z)

# Note 
- Android also has widget "AutoCompleteTextView" for same purpose , however in this project for demo sake Edit field is used 
 & logic for autocomplete is defined .
 
# Known bug
- On selection of word from suggestion list , the popup list reappears and user has to dismiss it by clicking/touching outside.


 
 
 
 

