package com.aevi.wordcomplete.presenter;

import com.aevi.wordcomplete.repository.WordRepository;
import com.aevi.wordcomplete.usecase.GetWordListUsecase;
import com.aevi.wordcomplete.view.StartupView;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.observers.TestObserver;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by abhiholkar on 24/06/2018.
 */
public class StartupPresenterTest {

    @Mock
    StartupView mockView;
    @Mock
    WordRepository mockRepository;
    @Mock
    GetWordListUsecase mockUsecase;

    StartupPresenter startupPresenter;

    @BeforeClass
    public static void before() {
        RxAndroidPlugins.reset();
        RxJavaPlugins.reset();
        RxJavaPlugins.setIoSchedulerHandler(new Function<Scheduler, Scheduler>() {
            @Override
            public Scheduler apply(@NonNull Scheduler scheduler) throws Exception {
                return Schedulers.trampoline();
            }
        });
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(new Function<Callable<Scheduler>, Scheduler>() {
            @Override
            public Scheduler apply(@NonNull Callable<Scheduler> schedulerCallable) throws Exception {
                return Schedulers.trampoline();
            }
        });
    }

    @AfterClass
    public static void after() {
        RxAndroidPlugins.reset();
        RxJavaPlugins.reset();
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        startupPresenter = new StartupPresenter(mockRepository, mockUsecase);

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void fetchWordList() throws Exception {
        TestObserver testObserver = new TestObserver();
        when(mockUsecase.fetchWords()).thenReturn(Observable.just(true));
        mockUsecase.fetchWords().subscribe(testObserver);
        startupPresenter.bindView(mockView);
        startupPresenter.fetchWordList();
        testObserver.assertNoErrors();
        verify(mockView).onFetchTaskComplete();
    }

    @Test
    public void fetchWordAndResult() throws Exception {
    }

    @Test
    public void bindView() throws Exception {
        startupPresenter.bindView(mockView);
        assertNotNull(mockView);
    }

}