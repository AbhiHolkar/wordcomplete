package com.aevi.wordcomplete.usecase;

import com.aevi.wordcomplete.repository.WordRepository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;

/**
 * Created by abhiholkar on 24/06/2018.
 */
public class GetWordListUsecaseTest {

    @Mock
    WordRepository wordRepository;
    GetWordListUsecase usecase;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        usecase = new GetWordListUsecase(wordRepository);

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void fetchWords() throws Exception {
        usecase.fetchWords();
        verify(wordRepository).fetchWordList();
        verify(wordRepository, only()).fetchWordList();

    }

}