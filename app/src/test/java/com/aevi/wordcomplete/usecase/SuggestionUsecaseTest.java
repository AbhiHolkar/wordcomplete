package com.aevi.wordcomplete.usecase;

import com.aevi.wordcomplete.repository.WordRepository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;

/**
 * Created by abhiholkar on 24/06/2018.
 */
public class SuggestionUsecaseTest {

    @Mock
    WordRepository wordRepository;
    SuggestionUsecase usecase;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        usecase = new SuggestionUsecase(wordRepository);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getSuggestionList() throws Exception {
        String test = "Te";
        usecase.getSuggestionList(test);
        verify(wordRepository).getSuggestions(test);
        verify(wordRepository, only()).getSuggestions(test);

    }

}