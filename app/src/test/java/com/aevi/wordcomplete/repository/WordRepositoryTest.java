package com.aevi.wordcomplete.repository;

import com.aevi.wordcomplete.model.WordList;
import com.aevi.wordcomplete.model.Words;
import com.aevi.wordcomplete.util.FileUtil;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by abhiholkar on 24/06/2018.
 */
public class WordRepositoryTest {

    @Mock
    FileUtil fileUtil;
    private WordRepository repository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        repository = new WordRepository(fileUtil);

    }

    @After
    public void tearDown() throws Exception {
        fileUtil = null;
        repository = null;
    }

    @Test
    public void fetchWordList() throws Exception {
        WordList mockWordList = mock(WordList.class);
        when(fileUtil.getWordFile()).thenReturn(mockWordList);
        repository.fetchWordList();
        assertNotNull(fileUtil);
        verify(fileUtil).getWordFile();
        assertTrue(repository.fetchWordList());
    }

    @Test
    public void fetchWordListWhenNull() throws Exception {
        when(fileUtil.getWordFile()).thenReturn(null);
        repository.fetchWordList();
        assertNotNull(fileUtil);
        assertFalse(repository.fetchWordList());
    }

    @Test
    public void getSubList() throws Exception {
        WordList mockWordList = mock(WordList.class);
        when(fileUtil.getWordFile()).thenReturn(mockWordList);
        Words mockedWords = mock(Words.class);
        List<String> testList = new ArrayList();
        testList.add("Tea");
        testList.add("Tear");
        testList.add("Tee");
        when(mockWordList.getWords()).thenReturn(mockedWords);
        when(mockedWords.getT()).thenReturn(testList);
        repository.getSubList("Te");
        assertEquals(repository.getSubList("Te"), testList);
    }

    @Test
    public void getSuggestions() throws Exception {
        /*WordList mockWordList = Mockito.mock(WordList.class);
        //To fix static using PowerMock
        when(fileUtil.getWordFile()).thenReturn(mockWordList);
        Words mockedWords = Mockito.mock(Words.class);
        repository.getSuggestions("");
        verify(fileUtil,never()).getWordFile();*/
    }

    @Test
    public void fiteredText() throws Exception {
    }

}