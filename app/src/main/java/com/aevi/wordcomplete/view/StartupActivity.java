package com.aevi.wordcomplete.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.aevi.wordcomplete.R;
import com.aevi.wordcomplete.app.WordCompleteApp;
import com.aevi.wordcomplete.presenter.StartupPresenter;
import com.aevi.wordcomplete.util.AppUtil;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class StartupActivity extends AppCompatActivity implements StartupView {

    @Inject
    StartupPresenter startupPresenter;
    boolean nonLaunherIntent;

    @Inject
    AppUtil appUtil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);
        ((WordCompleteApp) getApplication()).getAppComponent().inject(this);
        nonLaunherIntent = isLauncherIntent(getIntent());

    }

    private boolean isLauncherIntent(Intent intent) {
        return intent != null && intent.getType() != null && intent.getType().equals("text/plain");
    }

    @Override
    protected void onResume() {
        super.onResume();
        startupPresenter.bindView(this);
        if (nonLaunherIntent) {
            String text = getIntent().getStringExtra("AEVI_QUERY");
            startupPresenter.fetchWordAndResult(text);
        } else {
            startupPresenter.fetchWordList();
        }
    }

    @Override
    public void onFetchTaskComplete() {
        navigateToMainActivity();

    }

    @Override
    public void onFetchTaskIncomplete() {
        appUtil.setSuggestionAvailable(false);
        navigateToMainActivity();
    }

    @Override
    public void onFetchAndResultComplete(List<String> result) {
        Intent resultIntent = new Intent("com.aevi.RESULT_ACTION");
        resultIntent.putStringArrayListExtra("AEVI_RESULT", (ArrayList<String>) result);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public void onFetchAndResultFailure() {
        setResult(RESULT_CANCELED, null);
        finish();

    }

    public void navigateToMainActivity() {
        Intent nextIntent = new Intent(this, MainActivity.class);
        startActivity(nextIntent);
        finish();
    }


}
