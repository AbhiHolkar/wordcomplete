package com.aevi.wordcomplete.view;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.aevi.wordcomplete.R;
import com.aevi.wordcomplete.app.WordCompleteApp;
import com.aevi.wordcomplete.presenter.MainPresenter;
import com.aevi.wordcomplete.util.AppUtil;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements SuggestionView {

    @BindView(R.id.container)
    View container;

    @BindView(R.id.edit_text)
    EditText editTextView;

    @BindView(R.id.pb)
    ProgressBar pb;

    @Inject
    MainPresenter mainPresenter;

    @Inject
    AppUtil appUtil;

    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((WordCompleteApp) getApplication()).getAppComponent().inject(this);
        ButterKnife.bind(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mainPresenter.bindView(this);

        editTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (appUtil.isSuggestionAvailable() && s.length() > 1) {
                    mainPresenter.getSuggestion(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    @Override
    public void showSuggestion(List<String> suggestions) {
        Log.d(MainActivity.class.getSimpleName(), "Suggestions " + suggestions.toString());
        for (String str : suggestions) {
            Log.d(MainActivity.class.getSimpleName(), "Suggest : " + str);
        }
        if (!suggestions.isEmpty()) {
            showPopup(suggestions);
        }
    }

    @Override
    public void onSuggestionError() {

    }

    public void showPopup(final List<String> suggestions) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.suggest_text));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_singlechoice);
        adapter.addAll(suggestions);
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                editTextView.setText(suggestions.get(which));
                editTextView.setSelection(suggestions.get(which).length());
                editTextView.requestFocus();
            }
        });
        dialog = builder.show();
        dialog.setCanceledOnTouchOutside(true);

    }


}
