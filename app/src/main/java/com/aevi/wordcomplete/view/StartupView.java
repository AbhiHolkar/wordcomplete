package com.aevi.wordcomplete.view;

import java.util.List;

/**
 * Created by abhiholkar on 21/06/2018.
 */

public interface StartupView extends IView {

    void onFetchTaskComplete();

    void onFetchTaskIncomplete();

    void onFetchAndResultComplete(List<String> result);

    void onFetchAndResultFailure();
}
