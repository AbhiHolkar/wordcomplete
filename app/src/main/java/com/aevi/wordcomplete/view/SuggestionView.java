package com.aevi.wordcomplete.view;

import java.util.List;

/**
 * Created by abhiholkar on 21/06/2018.
 */

public interface SuggestionView extends IView {

    void showSuggestion(List<String> suggestions);

    void onSuggestionError();
}
