package com.aevi.wordcomplete.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WordList {

    @SerializedName("words")
    @Expose
    private Words words;

    public Words getWords() {
        return words;
    }

    public void setWords(Words words) {
        this.words = words;
    }

}