package com.aevi.wordcomplete.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Words {

    @SerializedName("A")
    @Expose
    private List<String> a = null;
    @SerializedName("B")
    @Expose
    private List<String> b = null;
    @SerializedName("C")
    @Expose
    private List<String> c = null;
    @SerializedName("D")
    @Expose
    private List<String> d = null;
    @SerializedName("E")
    @Expose
    private List<String> e = null;
    @SerializedName("F")
    @Expose
    private List<String> f = null;
    @SerializedName("G")
    @Expose
    private List<String> g = null;
    @SerializedName("H")
    @Expose
    private List<String> h = null;
    @SerializedName("I")
    @Expose
    private List<String> i = null;
    @SerializedName("J")
    @Expose
    private List<String> j = null;
    @SerializedName("K")
    @Expose
    private List<String> k = null;
    @SerializedName("L")
    @Expose
    private List<String> l = null;
    @SerializedName("M")
    @Expose
    private List<String> m = null;
    @SerializedName("N")
    @Expose
    private List<String> n = null;
    @SerializedName("O")
    @Expose
    private List<String> o = null;
    @SerializedName("P")
    @Expose
    private List<String> p = null;
    @SerializedName("Q")
    @Expose
    private List<String> q = null;
    @SerializedName("R")
    @Expose
    private List<String> r = null;
    @SerializedName("S")
    @Expose
    private List<String> s = null;
    @SerializedName("T")
    @Expose
    private List<String> t = null;
    @SerializedName("U")
    @Expose
    private List<String> u = null;
    @SerializedName("V")
    @Expose
    private List<String> v = null;
    @SerializedName("W")
    @Expose
    private List<String> w = null;
    @SerializedName("X")
    @Expose
    private List<String> x = null;
    @SerializedName("Y")
    @Expose
    private List<String> y = null;
    @SerializedName("Z")
    @Expose
    private List<String> z = null;

    public List<String> getA() {
        return a;
    }

    public void setA(List<String> a) {
        this.a = a;
    }

    public List<String> getB() {
        return b;
    }

    public void setB(List<String> b) {
        this.b = b;
    }

    public List<String> getC() {
        return c;
    }

    public void setC(List<String> c) {
        this.c = c;
    }

    public List<String> getD() {
        return d;
    }

    public void setD(List<String> d) {
        this.d = d;
    }

    public List<String> getE() {
        return e;
    }

    public void setE(List<String> e) {
        this.e = e;
    }

    public List<String> getF() {
        return f;
    }

    public void setF(List<String> f) {
        this.f = f;
    }

    public List<String> getG() {
        return g;
    }

    public void setG(List<String> g) {
        this.g = g;
    }

    public List<String> getH() {
        return h;
    }

    public void setH(List<String> h) {
        this.h = h;
    }

    public List<String> getI() {
        return i;
    }

    public void setI(List<String> i) {
        this.i = i;
    }

    public List<String> getJ() {
        return j;
    }

    public void setJ(List<String> j) {
        this.j = j;
    }

    public List<String> getK() {
        return k;
    }

    public void setK(List<String> k) {
        this.k = k;
    }

    public List<String> getL() {
        return l;
    }

    public void setL(List<String> l) {
        this.l = l;
    }

    public List<String> getM() {
        return m;
    }

    public void setM(List<String> m) {
        this.m = m;
    }

    public List<String> getN() {
        return n;
    }

    public void setN(List<String> n) {
        this.n = n;
    }

    public List<String> getO() {
        return o;
    }

    public void setO(List<String> o) {
        this.o = o;
    }

    public List<String> getP() {
        return p;
    }

    public void setP(List<String> p) {
        this.p = p;
    }

    public List<String> getQ() {
        return q;
    }

    public void setQ(List<String> q) {
        this.q = q;
    }

    public List<String> getR() {
        return r;
    }

    public void setR(List<String> r) {
        this.r = r;
    }

    public List<String> getS() {
        return s;
    }

    public void setS(List<String> s) {
        this.s = s;
    }

    public List<String> getT() {
        return t;
    }

    public void setT(List<String> t) {
        this.t = t;
    }

    public List<String> getU() {
        return u;
    }

    public void setU(List<String> u) {
        this.u = u;
    }

    public List<String> getV() {
        return v;
    }

    public void setV(List<String> v) {
        this.v = v;
    }

    public List<String> getW() {
        return w;
    }

    public void setW(List<String> w) {
        this.w = w;
    }

    public List<String> getX() {
        return x;
    }

    public void setX(List<String> x) {
        this.x = x;
    }

    public List<String> getY() {
        return y;
    }

    public void setY(List<String> y) {
        this.y = y;
    }

    public List<String> getZ() {
        return z;
    }

    public void setZ(List<String> z) {
        this.z = z;
    }
}
