package com.aevi.wordcomplete.app;

import android.app.Application;

import com.aevi.wordcomplete.di.component.AppComponent;
import com.aevi.wordcomplete.di.component.DaggerAppComponent;
import com.aevi.wordcomplete.di.module.AppModule;

/**
 * Created by abhiholkar on 22/06/2018.
 */

public class WordCompleteApp extends Application {

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }


}
