package com.aevi.wordcomplete.presenter;

import com.aevi.wordcomplete.view.IView;

/**
 * Created by abhiholkar on 22/06/2018.
 */

public abstract class BasePresenter {
    public abstract void bindView(IView view);
}
