package com.aevi.wordcomplete.presenter;

import com.aevi.wordcomplete.repository.WordRepository;
import com.aevi.wordcomplete.usecase.GetWordListUsecase;
import com.aevi.wordcomplete.view.IView;
import com.aevi.wordcomplete.view.StartupView;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by abhiholkar on 21/06/2018.
 */

public class StartupPresenter extends BasePresenter {

    WordRepository wordRepository;
    GetWordListUsecase getWordListUsecase;
    private StartupView startupView;


    @Inject
    public StartupPresenter(WordRepository wordRepository, GetWordListUsecase usecase) {
        this.wordRepository = wordRepository;
        this.getWordListUsecase = usecase;
    }

    public void fetchWordList() {
        getWordListUsecase.fetchWords().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableObserver<Boolean>() {
            @Override
            public void onNext(Boolean result) {
                if (result) {
                    startupView.onFetchTaskComplete();
                } else {
                    startupView.onFetchTaskIncomplete();
                }


            }

            @Override
            public void onError(Throwable e) {
                startupView.onFetchTaskIncomplete();

            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void fetchWordAndResult(final String enteredText) {
        getWordListUsecase.fetchWords().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new DisposableObserver<Boolean>() {
            @Override
            public void onNext(Boolean success) {
                if (success) {
                    List<String> resultList = wordRepository.getSuggestions(enteredText);
                    if (resultList != null && !resultList.isEmpty()) {
                        startupView.onFetchAndResultComplete(resultList);
                    } else {
                        startupView.onFetchAndResultFailure();
                    }
                } else {
                    startupView.onFetchAndResultFailure();
                }

            }

            @Override
            public void onError(Throwable e) {
                startupView.onFetchAndResultFailure();

            }

            @Override
            public void onComplete() {

            }
        });

    }


    @Override
    public void bindView(IView view) {
        this.startupView = (StartupView) view;
    }
}
