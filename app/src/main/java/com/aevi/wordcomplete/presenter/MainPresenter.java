package com.aevi.wordcomplete.presenter;

import com.aevi.wordcomplete.repository.WordRepository;
import com.aevi.wordcomplete.usecase.SuggestionUsecase;
import com.aevi.wordcomplete.view.IView;
import com.aevi.wordcomplete.view.SuggestionView;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by abhiholkar on 21/06/2018.
 */

public class MainPresenter extends BasePresenter {

    SuggestionView suggestionView;
    WordRepository repository;
    SuggestionUsecase suggestionUsecase;

    @Inject
    public MainPresenter(WordRepository repository, SuggestionUsecase usecase) {
        this.repository = repository;
        this.suggestionUsecase = usecase;

    }


    public void getSuggestion(String enteredText) {
        suggestionUsecase.getSuggestionList(enteredText).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<List<String>>() {
                    @Override
                    public void onNext(List<String> strings) {
                        suggestionView.showSuggestion(strings);

                    }

                    @Override
                    public void onError(Throwable e) {
                        suggestionView.onSuggestionError();

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public void bindView(IView view) {
        suggestionView = (SuggestionView) view;
    }
}
