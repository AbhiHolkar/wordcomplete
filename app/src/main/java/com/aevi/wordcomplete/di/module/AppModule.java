package com.aevi.wordcomplete.di.module;

import android.app.Application;
import android.content.Context;

import com.aevi.wordcomplete.presenter.MainPresenter;
import com.aevi.wordcomplete.presenter.StartupPresenter;
import com.aevi.wordcomplete.repository.WordRepository;
import com.aevi.wordcomplete.usecase.GetWordListUsecase;
import com.aevi.wordcomplete.usecase.SuggestionUsecase;
import com.aevi.wordcomplete.util.AppUtil;
import com.aevi.wordcomplete.util.FileUtil;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by abhiholkar on 22/06/2018.
 */

@Module
public class AppModule {

    Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Application providesApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    Context providesContext() {
        return mApplication.getApplicationContext();
    }

    @Provides
    @Singleton
    WordRepository providesWordRepository(FileUtil fileUtil) {
        return new WordRepository(fileUtil);
    }

    @Provides
    @Singleton
    AppUtil providesAppUtil() {
        return new AppUtil();
    }

    @Provides
    GetWordListUsecase providesGetWordListUse(WordRepository wordRepository) {
        return new GetWordListUsecase(wordRepository);
    }

    @Provides
    SuggestionUsecase providesSuggestionUsecase(WordRepository wordRepository) {
        return new SuggestionUsecase(wordRepository);
    }


    @Provides
    MainPresenter providesMainPresenter(WordRepository wordRepository, SuggestionUsecase suggestionUsecase) {
        return new MainPresenter(wordRepository, suggestionUsecase);
    }

    @Provides
    StartupPresenter providesStartupPresenter(WordRepository wordRepository, GetWordListUsecase getWordListUsecase) {
        return new StartupPresenter(wordRepository, getWordListUsecase);
    }


}
