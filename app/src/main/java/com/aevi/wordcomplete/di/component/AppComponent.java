package com.aevi.wordcomplete.di.component;

import com.aevi.wordcomplete.di.module.AppModule;
import com.aevi.wordcomplete.view.MainActivity;
import com.aevi.wordcomplete.view.StartupActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by abhiholkar on 22/06/2018.
 */

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(StartupActivity startupActivity);

    void inject(MainActivity mainActivity);
}
