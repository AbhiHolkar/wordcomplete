package com.aevi.wordcomplete.usecase;

import com.aevi.wordcomplete.repository.WordRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by abhiholkar on 21/06/2018.
 */

public class SuggestionUsecase {

    WordRepository wordRepository;

    @Inject
    public SuggestionUsecase(WordRepository wordRepository) {
        this.wordRepository = wordRepository;
    }

    public Observable<List<String>> getSuggestionList(String enteredText) {
        return Observable.just(wordRepository.getSuggestions(enteredText));
    }
}
