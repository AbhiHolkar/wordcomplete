package com.aevi.wordcomplete.usecase;

import com.aevi.wordcomplete.repository.WordRepository;

import javax.inject.Inject;

import io.reactivex.Observable;


/**
 * Created by abhiholkar on 21/06/2018.
 */

public class GetWordListUsecase {

    WordRepository wordRepository;

    @Inject
    public GetWordListUsecase(WordRepository repository) {
        wordRepository = repository;
    }

    public Observable<Boolean> fetchWords() {
        return Observable.just(wordRepository.fetchWordList());
    }


}
