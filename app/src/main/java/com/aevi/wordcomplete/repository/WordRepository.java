package com.aevi.wordcomplete.repository;

import android.support.annotation.VisibleForTesting;
import android.text.TextUtils;

import com.aevi.wordcomplete.model.WordList;
import com.aevi.wordcomplete.model.Words;
import com.aevi.wordcomplete.util.FileUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by abhiholkar on 21/06/2018.
 */

public class WordRepository {


    FileUtil fileUtil;
    WordList wordList;

    @Inject
    public WordRepository(FileUtil fileUtil) {
        this.fileUtil = fileUtil;
    }

    public boolean fetchWordList() {
        wordList = fileUtil.getWordFile();
        return wordList != null;
    }

    @VisibleForTesting
    public List<String> getSubList(String enteredText) {

        List<String> suggestion = Collections.emptyList();
        Words words = fileUtil.getWordFile().getWords();
        char character = Character.toUpperCase(enteredText.charAt(0));
        switch (character) {
            case 'A':
                suggestion = (words.getA());
                break;
            case 'B':
                suggestion = (words.getB());
                break;
            case 'C':
                suggestion = (words.getC());
                break;
            case 'D':
                suggestion = (words.getD());
                break;
            case 'E':
                suggestion = (words.getE());
                break;
            case 'F':
                suggestion = (words.getF());
                break;
            case 'G':
                suggestion = (words.getG());
                break;
            case 'H':
                suggestion = (words.getH());
                break;
            case 'I':
                suggestion = (words.getI());
                break;
            case 'K':
                suggestion = (words.getK());
                break;
            case 'L':
                suggestion = (words.getL());
                break;
            case 'M':
                suggestion = (words.getM());
                break;
            case 'N':
                suggestion = (words.getN());
                break;
            case 'O':
                suggestion = (words.getO());
                break;
            case 'P':
                suggestion = (words.getP());
                break;
            case 'Q':
                suggestion = (words.getQ());
                break;
            case 'R':
                suggestion = (words.getR());
                break;
            case 'S':
                suggestion = (words.getS());
                break;
            case 'T':
                suggestion = (words.getT());
                break;
            case 'U':
                suggestion = (words.getU());
                break;
            case 'V':
                suggestion = (words.getV());
                break;
            case 'W':
                suggestion = (words.getW());
                break;
            case 'X':
                suggestion = (words.getX());
                break;
            case 'Y':
                suggestion = (words.getY());
                break;
            case 'Z':
                suggestion = (words.getZ());
                break;

        }
        return suggestion;
    }


    public List<String> getSuggestions(String enteredText) {
        if (TextUtils.isEmpty(enteredText)) return null;

        List<String> subset = getSubList(enteredText);
        if (subset == null || subset.isEmpty()) return null;
        return fiteredText(subset, enteredText);

    }

    @VisibleForTesting
    public List<String> fiteredText(List<String> subset, String enteredText) {
        //Sort alphabetcially
        Collections.sort(subset);
        List<String> suggestionList = new ArrayList<>();
        for (String subsetString : subset) {
            if (subsetString.startsWith(enteredText.toLowerCase()) || subsetString.startsWith(enteredText.toUpperCase())) {
                suggestionList.add(subsetString);
            }
        }
        return suggestionList;
    }

}
