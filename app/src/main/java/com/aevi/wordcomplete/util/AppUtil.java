package com.aevi.wordcomplete.util;

import javax.inject.Inject;

/**
 * Created by abhiholkar on 24/06/2018.
 */

public class AppUtil {

    private boolean isSuggestionAvailable = true;

    @Inject
    public AppUtil() {
    }

    public boolean isSuggestionAvailable() {
        return isSuggestionAvailable;
    }

    public void setSuggestionAvailable(boolean suggestionAvailable) {
        isSuggestionAvailable = suggestionAvailable;
    }


}
